<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::get('/', 'SessionsController@create');

Route::get('logout', 'SessionsController@destroy');

Route::resource('users', 'UsersController');
Route::resource('sessions', 'SessionsController');

Route::get('step1', function () {
    if (Auth::check()) {
        return Redirect::to('/');
    }
    return View::make('reg.step1');
});

Route::post('step1', function () {

    $rules = [
        'username' => 'required|min:2|unique:users,username',
        'password' => array(
            'required',
            'between:6,12',
            'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
        ),
        'email' => 'required|email|unique:users,email',
    ];

    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {
        return Redirect::to('step1')->withInput()->withErrors($validator);
    }
    return Redirect::to('step2')->withInput();
});

Route::get('step2', function () {
    if (Auth::check()) {
        return Redirect::to('/');
    }
    return View::make('reg.step2');
});

Route::post('step2', function () {

    $rules = [
        'firstname' => 'required|min:2|regex:/^[a-zA-Z "]+$/',
        'lastname' => 'required|min:2|regex:/^[a-zA-Z "]+$/',
    ];

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) {
        return Redirect::to('step2')->withInput()->withErrors($validator);
    }
    return Redirect::to('step3')->withInput();
});

Route::get('step3', function () {
    if (Auth::check()) {
        return Redirect::to('/');
    }
    return View::make('reg.step3');
});
