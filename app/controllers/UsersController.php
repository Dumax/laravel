<?php

class UsersController extends \BaseController
{

    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $user = new User;

        $user->username = Input::get('username');
        $user->password = Hash::make(Input::get('password'));
        $user->email = Input::get('email');
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->gender = Input::get('gender');
        $user->about = Input::get('about');
        $user->save();

        if (Auth::check()) {
            Auth::logout();
        }

        Auth::attempt(Input::only('email', 'password'));
        return Redirect::to("users/show");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {
        $user = Auth::user();

        return View::make('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = Auth::user();

        return View::make('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update()
    {

        $user = Auth::user();

        Validator::extend('passcheck', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->password); // Works for any form!
        });

        $messages = [
            'passcheck' => 'Your old password was incorrect',
        ];

        $rules = [
            'username' => 'min:2',
            'oldpass' => 'passcheck|required_with:password',
            'password' => array(
                'between:6,12',
                'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
            ),
            'email' => 'email',
            'firstname' => 'required|min:2|regex:/^[a-zA-Z "]+$/',
            'lastname' => 'required|min:2|regex:/^[a-zA-Z "]+$/',
        ];

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $user->username = Input::get('username');
        if (!Input::get('password') == 0) {
            $user->password = Hash::make(Input::get('password'));
        }
        $user->email = Input::get('email');
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->gender = Input::get('gender');
        $user->about = Input::get('about');
        $user->save();

        return Redirect::to('users/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
