@extends('layouts.default')

@section('content')
	<h1>User profile:</h1>
	<table class="table table-user-information">
			<tbody>
				<tr>
					<td>Username:</td>
					<td>{{ $user->username }}</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td>{{ $user->email }}</td>
				</tr>
				<tr>
					<td>First name</td>
					<td>{{ $user->firstname }}</td>
				</tr>
				<tr>
					<td>Last name</td>
					<td>{{ $user->lastname }}</td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td>{{ $user->gender }}</td>
				</tr>
				<tr>
					<td>About:</td>
					<td>{{ $user->about }}</td>
				</tr>
			</tbody>
		</table>
	{{ link_to("logout",'logout', array('class' => 'btn btn-primary')) }}
    {{ link_to("users/{$user->username}/edit",'edit profile', array('class' => 'btn btn-success')) }}
@stop
