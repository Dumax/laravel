@extends('layouts.default')

@section('content')
	<h1>Edit profile</h1>
	{{ Form::model($user, array('action' => array('UsersController@update', $user->id), 'method' => 'PUT')) }}
		<div class="form-group">
			{{ Form::label('username','Username:',array('for' => 'username')) }}
			{{ Form::text('username', null, array('class' => 'form-control')) }}
			{{ $errors->first('username','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('oldpass','Old password:', array('for' => 'oldpass')) }}
			{{ Form::password('oldpass', array('class' => 'form-control')) }}
			{{ $errors->first('oldpass','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('password','New password:', array('for' => 'username')) }}
			{{ Form::password('password', array('class' => 'form-control')) }}
			{{ $errors->first('password','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('email','Email:') }}
			{{ Form::text('email', null, array('class' => 'form-control','id' => 'pwd')) }}
			{{ $errors->first('email','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('firstname','First name:',array('for' => 'firstname')) }}
			{{ Form::text('firstname', null, array('class' => 'form-control')) }}
			{{ $errors->first('firstname','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('lastname','Last name:',array('for' => 'lastname')) }}
			{{ Form::text('lastname', null, array('class' => 'form-control')) }}
			{{ $errors->first('lastname','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('gender','Gender:') }}
			{{ Form::select('gender', array('male' => 'male', 'female' => 'female'), null, array('class' => 'form-control')) }}

		</div>
		<div class="form-group">
			{{ Form::label('about','About:') }}
			{{ Form::textarea('about', null, array(
			    'id'      => 'about',
			    'rows'    => 10,
			    'class'   => 'form-control',
			)) }}
		</div>
		{{ link_to("users/show",'Cancel', array('class' => 'btn btn-danger')) }}
		{{ Form::submit('Update profile', array('class' => 'btn btn-success')) }}
	{{ Form::close() }}
@stop
