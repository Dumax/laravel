@extends('layouts.default')

@section('content')
	<h1>Login</h1>
	{{ Form::open(['route' => 'sessions.store']) }}
		<div class="form-group">
			{{ Form::label('email','Email:') }}
			{{ Form::text('email', null, array('class' => 'form-control','id' => 'pwd')) }}
			{{ $errors->first('email','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('password','Password:', array('for' => 'username')) }}
			{{ Form::password('password', array('class' => 'form-control')) }}
			{{ $errors->first('password','<p class="text-danger">:message</p>') }}
		</div>
		{{ Form::submit('Login', array('class' => 'btn btn-primary')) }}
		{{ link_to("step1",'registration', array('class' => 'btn btn-success')) }}
	{{ Form::close() }}

@stop
