@extends('layouts.default')

@section('content')
	<h1>Registration - Secure info</h1>
	{{ Form::open(['url' => 'step1']) }}
		<div class="form-group">
			{{ Form::label('username','Username:',array('for' => 'username')) }}
			{{ Form::text('username', null, array('class' => 'form-control')) }}
			{{ $errors->first('username','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('password','Password:', array('for' => 'username')) }}
			{{ Form::password('password', array('class' => 'form-control')) }}
			{{ $errors->first('password','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('email','Email:') }}
			{{ Form::text('email', null, array('class' => 'form-control','id' => 'pwd')) }}
			{{ $errors->first('email','<p class="text-danger">:message</p>') }}
		</div>
		{{ link_to("/",'Back', array('class' => 'btn btn-default')) }}
		{{ Form::submit('Next', array('class' => 'btn btn-default')) }}
	{{ Form::close() }}
@stop
