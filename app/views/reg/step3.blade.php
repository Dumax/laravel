@extends('layouts.default')

@section('content')
	<h1>Registration - Checking</h1>
	<h3>Сheck the entered information  before confirm</h3>
	{{ Form::open(array('route' => 'users.store')) }}
		{{ Form::hidden('username',"Input::old('username')") }}
		{{ Form::hidden('email',"Input::old('email')") }}
		{{ Form::hidden('password',"Input::old('password')") }}
		{{ Form::hidden('firstname',"Input::old('firstname')") }}
		{{ Form::hidden('lastname',"Input::old('lastname')") }}
		{{ Form::hidden('gender',"Input::old('gender')") }}
		{{ Form::hidden('about',"Input::old('about')") }}
		<table class="table table-user-information">
			<tbody>
				<tr>
					<td>Username:</td>
					<td>{{ Input::old('username') }}</td>
				</tr>
				<tr>
					<td>Password:</td>
					<td>{{ Input::old('password') }}</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td>{{ Input::old('email') }}</td>
				</tr>
				<tr>
					<td>First name</td>
					<td>{{ Input::old('firstname') }}</td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td>{{ Input::old('gender') }}</td>
				</tr>
				<tr>
					<td>About:</td>
					<td>{{ Input::old('about') }}</td>
				</tr>
			</tbody>
		</table>
		{{ link_to("step2",'Back', array('class' => 'btn btn-default')) }}
		{{ Form::submit('Registrate', array('class' => 'btn btn-default')) }}
	{{ Form::close() }}
@stop
