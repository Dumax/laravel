@extends('layouts.default')

@section('content')
	<h1>Registration - Personal info</h1>
	{{ Form::open(['url' => 'step2']) }}
		{{ Form::hidden('username',"Input::old('username')") }}
		{{ Form::hidden('email',"Input::old('email')") }}
		{{ Form::hidden('password',"Input::old('password')") }}
		<div class="form-group">
			{{ Form::label('firstname','First name:',array('for' => 'firstname')) }}
			{{ Form::text('firstname', null, array('class' => 'form-control')) }}
			{{ $errors->first('firstname','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('lastname','Last name:',array('for' => 'lastname')) }}
			{{ Form::text('lastname', null, array('class' => 'form-control')) }}
			{{ $errors->first('lastname','<p class="text-danger">:message</p>') }}
		</div>
		<div class="form-group">
			{{ Form::label('gender','Gender:') }}
			{{ Form::select('gender', array('male' => 'male', 'female' => 'female'), null, array('class' => 'form-control')) }}

		</div>
		<div class="form-group">
			{{ Form::label('about','About:') }}
			{{ Form::textarea('about', null, array(
			    'id'      => 'about',
			    'rows'    => 10,
			    'class'   => 'form-control',
			)) }}

		</div>
		{{ link_to("step1",'Back', array('class' => 'btn btn-default')) }}
		{{ Form::submit('Next', array('class' => 'btn btn-default')) }}
	{{ Form::close() }}

@stop



<!-- <div class="form-group">
			{{ Form::label('email','Email:') }}
			{{ Form::text('email', null, array('class' => 'form-control','id' => 'pwd')) }}
			{{ $errors->first('email','<p class="text-danger">:message</p>') }}
		</div>
		{{ link_to("/",'Back') }}
		<div>{{ Form::submit('Next', array('class' => 'btn btn-default')) }}</div>
 -->
